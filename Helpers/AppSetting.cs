using System;

namespace sakuko_api.Helpers
{
    public class AppSettings
    {
        public const string SectionName = "SakukoPartner";
        public string Username { get; set; }
        public string Password { get; set; }
        
    }
}