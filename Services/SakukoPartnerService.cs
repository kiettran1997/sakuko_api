using System.Net;
using System.Text;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using sakuko_api.DTO;
using sakuko_api.Utilities;

namespace sakuko_api.Services;

public class SakukoPartnerService
{
    public static SakukoPartnerService instance { get; set; }
    public static SakukoPartnerService Instance()
    {
        if (instance == null) instance = new SakukoPartnerService();
        return instance;
    }

    public string GetUsername(string userName)
    {
        return userName;
    }

    public async Task<PostPO_ResultResponse> SendGoodReceipt(string xmlString)
    {
        PostPO_ResultResponse poResponse = new PostPO_ResultResponse();
        try
        {
            // Create the HttpClient instance with authentication
            HttpClientHandler handler = new HttpClientHandler
            {
                Credentials = new NetworkCredential(UtilityService.Username, UtilityService.Password)
            };
            using (HttpClient httpClient = new HttpClient(handler))
            {
                // Create the HttpRequestMessage and set the headers
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, UtilityService.SoapEndpoint);
                request.Content = new StringContent(xmlString, Encoding.UTF8, "text/xml");

                // Send the request and get the response
                HttpResponseMessage response = httpClient.SendAsync(request).GetAwaiter().GetResult();
                Console.WriteLine("----------response HTTP---------");
                Console.WriteLine(response);
                Console.WriteLine("-------------------");
                // Check if the request was successful (HTTP status code 200)
                if (response.IsSuccessStatusCode)
                {
                    // Read the SOAP response
                    string soapResponse = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    //Console.WriteLine(soapResponse);
                    Console.WriteLine("----------SOAP Response---------");
                    Console.WriteLine(soapResponse);
                    Console.WriteLine("-------------------");

                    // Process the SOAP response (parsing, etc.)
                    // For example, you can use XmlDocument or XDocument to parse the XML response
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(soapResponse);

                    // Find the PostPO_Result node and extract its value
                    XmlNode postPOResultNode = xmlDoc.SelectSingleNode("/PostPO_Result");

                    Console.WriteLine("----------postPOResultNode---------");
                    Console.WriteLine(postPOResultNode);
                    Console.WriteLine("-------------------");

                    if (postPOResultNode != null)
                    {
                        string postPOResultValue = postPOResultNode.InnerText;
                        Console.WriteLine("----------SUCCESS---------");
                        Console.WriteLine("PostPO_Result: " + postPOResultValue);
                        Console.WriteLine("-------------------");
                        poResponse.Code = 1;
                        poResponse.Message = "PostPO_Result: " + postPOResultValue;
                    }
                    else
                    {
                        Console.WriteLine("PostPO_Result not found in SOAP response.");
                        poResponse.StatusCode = response.StatusCode;
                        poResponse.Code = 0;
                        poResponse.Message = "PostPO_Result not found in SOAP response";
                    }
                }
                else
                {
                    // Handle the case when the request was not successful
                    //Console.WriteLine("SOAP Request Failed. Status Code: " + response.StatusCode);
                    Console.WriteLine("----------FAILED---------");
                    Console.WriteLine("SOAP Request Failed. Status Code: " + response.StatusCode);
                    Console.WriteLine("-------------------");
                    poResponse.StatusCode = response.StatusCode;
                    poResponse.Code = 0;
                    poResponse.Message = "SOAP Request Failed. Status Code: " + response.StatusCode;
                }
            }
            return poResponse;
        }
        catch (Exception ex)
        {
            // Handle exceptions appropriately
            //Console.WriteLine("Error: " + ex.Message);
            Console.WriteLine("----------FAILED---------");
            Console.WriteLine("Error: " + ex.Message);
            Console.WriteLine("-------------------");
            throw ex;
        }
    }
}