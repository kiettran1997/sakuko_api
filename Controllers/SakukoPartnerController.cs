using System.Xml;
using Microsoft.AspNetCore.Mvc;
using sakuko_api.Services;
using System.Net;
using System.Text;
using sakuko_api.DTO;
using System.Web;
using sakuko_api.Utilities;

namespace sakuko_api.Controllers;

[ApiController]
[Route("api/[controller]")]
public class SakukoPartnerController : ControllerBase
{
    private readonly ILogger<SakukoPartnerController> _logger;
    private readonly SakukoPartnerService _sakukoPartnerService;

    public SakukoPartnerController(ILogger<SakukoPartnerController> logger, SakukoPartnerService sakukoPartnerService)
    {
        _logger = logger;
        _sakukoPartnerService = sakukoPartnerService;
    }

    [HttpGet]
    [Route("get")]
    public IActionResult GetUsername()
    {
        Console.WriteLine("****** Start App Sakuko *****");
        string soapEndpoint = "http://navtest.sakuko.vn:7047/NAV_TEST_01/WS/FC_VPH_01/Codeunit/SMLDataSync"; // Replace with the actual SOAP endpoint URL
        string username = "nav"; // Replace with the actual username
        string password = "Sakuko2006"; // Replace with the actual password

        // Create the SOAP request body with PONo parameter
        string soapRequestBody = $@"
                <definitions xmlns=""http://schemas.xmlsoap.org/wsdl/"" xmlns:tns=""urn:microsoft-dynamics-schemas/page/smlsync"" targetNamespace=""urn:microsoft-dynamics-schemas/page/smlsync"">
    <types>
        <xsd:schema xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" elementFormDefault=""qualified"" targetNamespace=""urn:microsoft-dynamics-schemas/page/smlsync"">
            <xsd:simpleType name=""Transaction_Type"">
                <xsd:restriction base=""xsd:string"">
                    <xsd:enumeration value=""UpdateStatus""/>
                    <xsd:enumeration value=""PostDocument""/>
                </xsd:restriction>
            </xsd:simpleType>
            <xsd:simpleType name=""Document_Type"">
                <xsd:restriction base=""xsd:string"">
                    <xsd:enumeration value=""SO""/>
                    <xsd:enumeration value=""SRO""/>
                    <xsd:enumeration value=""PO"">1<xsd:enumeration>
                    <xsd:enumeration value=""PRO""/>
                    <xsd:enumeration value=""TO""/>
                </xsd:restriction>
            </xsd:simpleType>
            <xsd:complexType name=""SMLSync"">
                <xsd:sequence>
                    <xsd:element minOccurs=""0"" maxOccurs=""1"" name=""Key"" type=""xsd:string""/>
                    <xsd:element minOccurs=""0"" maxOccurs=""1"" name=""Transaction_Type"" type=""tns:Transaction_Type""/>
                    <xsd:element minOccurs=""0"" maxOccurs=""1"" name=""Document_Type"" type=""tns:Document_Type"">1234567</xsd:element>
                    <xsd:element minOccurs=""0"" maxOccurs=""1"" name=""Document_No"" type=""xsd:string"">1431423423</xsd:element>
                    <xsd:element minOccurs=""0"" maxOccurs=""1"" name=""Location_Code"" type=""xsd:string""/>
                    <xsd:element minOccurs=""0"" maxOccurs=""1"" name=""Status_Code"" type=""xsd:string""/>
                    <xsd:element minOccurs=""0"" maxOccurs=""1"" name=""Posting_Date"" type=""xsd:date""/>
                    <xsd:element minOccurs=""0"" maxOccurs=""1"" name=""Item_No"" type=""xsd:string""/>
                    <xsd:element minOccurs=""0"" maxOccurs=""1"" name=""UOM"" type=""xsd:string""/>
                    <xsd:element minOccurs=""0"" maxOccurs=""1"" name=""Quantity"" type=""xsd:decimal""/>
                    <xsd:element minOccurs=""0"" maxOccurs=""1"" name=""To_Location"" type=""xsd:string""/>
                </xsd:sequence>
            </xsd:complexType>
            <xsd:complexType name=""SMLSync_List"">
                <xsd:sequence>
                    <xsd:element minOccurs=""1"" maxOccurs=""unbounded"" name=""SMLSync"" type=""tns:SMLSync""/>
                </xsd:sequence>
            </xsd:complexType>
            <xsd:simpleType name=""SMLSync_Fields"">
                <xsd:restriction base=""xsd:string"">
                    <xsd:enumeration value=""Transaction_Type""/>
                    <xsd:enumeration value=""Document_Type""/>
                    <xsd:enumeration value=""Document_No""/>
                    <xsd:enumeration value=""Location_Code""/>
                    <xsd:enumeration value=""Status_Code""/>
                    <xsd:enumeration value=""Posting_Date""/>
                    <xsd:enumeration value=""Item_No""/>
                    <xsd:enumeration value=""UOM""/>
                    <xsd:enumeration value=""Quantity""/>
                    <xsd:enumeration value=""To_Location""/>
                </xsd:restriction>
            </xsd:simpleType>
            <xsd:complexType name=""SMLSync_Filter"">
                <xsd:sequence>
                    <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""Field"" type=""tns:SMLSync_Fields""/>
                    <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""Criteria"" type=""xsd:string""/>
                </xsd:sequence>
            </xsd:complexType>
            <xsd:element name=""ReadByRecId"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""recId"" type=""xsd:string""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""ReadByRecId_Result"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""0"" maxOccurs=""1"" name=""SMLSync"" type=""tns:SMLSync""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""ReadMultiple"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""1"" maxOccurs=""unbounded"" name=""filter"" type=""tns:SMLSync_Filter""/>
                        <xsd:element minOccurs=""0"" maxOccurs=""1"" name=""bookmarkKey"" type=""xsd:string""/>
                        <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""setSize"" type=""xsd:int""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""ReadMultiple_Result"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""0"" maxOccurs=""1"" name=""ReadMultiple_Result"" type=""tns:SMLSync_List""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""IsUpdated"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""Key"" type=""xsd:string""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""IsUpdated_Result"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""IsUpdated_Result"" type=""xsd:boolean""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""GetRecIdFromKey"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""Key"" type=""xsd:string""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""GetRecIdFromKey_Result"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""GetRecIdFromKey_Result"" type=""xsd:string""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""Create"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""SMLSync"" type=""tns:SMLSync""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""Create_Result"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""SMLSync"" type=""tns:SMLSync""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""CreateMultiple"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""SMLSync_List"" type=""tns:SMLSync_List""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""CreateMultiple_Result"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""SMLSync_List"" type=""tns:SMLSync_List""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""Update"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""SMLSync"" type=""tns:SMLSync""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""Update_Result"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""SMLSync"" type=""tns:SMLSync""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""UpdateMultiple"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""SMLSync_List"" type=""tns:SMLSync_List""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""UpdateMultiple_Result"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""SMLSync_List"" type=""tns:SMLSync_List""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""Delete"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""Key"" type=""xsd:string""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
            <xsd:element name=""Delete_Result"">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""Delete_Result"" type=""xsd:boolean""/>
                    </xsd:sequence>
                </xsd:complexType>
            </xsd:element>
        </xsd:schema>
    </types>
    <message name=""ReadByRecId"">
        <part name=""parameters"" element=""tns:ReadByRecId""/>
    </message>
    <message name=""ReadByRecId_Result"">
        <part name=""parameters"" element=""tns:ReadByRecId_Result""/>
    </message>
    <message name=""ReadMultiple"">
        <part name=""parameters"" element=""tns:ReadMultiple""/>
    </message>
    <message name=""ReadMultiple_Result"">
        <part name=""parameters"" element=""tns:ReadMultiple_Result""/>
    </message>
    <message name=""IsUpdated"">
        <part name=""parameters"" element=""tns:IsUpdated""/>
    </message>
    <message name=""IsUpdated_Result"">
        <part name=""parameters"" element=""tns:IsUpdated_Result""/>
    </message>
    <message name=""GetRecIdFromKey"">
        <part name=""parameters"" element=""tns:GetRecIdFromKey""/>
    </message>
    <message name=""GetRecIdFromKey_Result"">
        <part name=""parameters"" element=""tns:GetRecIdFromKey_Result""/>
    </message>
    <message name=""Create"">
        <part name=""parameters"" element=""tns:Create""/>
    </message>
    <message name=""Create_Result"">
        <part name=""parameters"" element=""tns:Create_Result""/>
    </message>
    <message name=""CreateMultiple"">
        <part name=""parameters"" element=""tns:CreateMultiple""/>
    </message>
    <message name=""CreateMultiple_Result"">
        <part name=""parameters"" element=""tns:CreateMultiple_Result""/>
    </message>
    <message name=""Update"">
        <part name=""parameters"" element=""tns:Update""/>
    </message>
    <message name=""Update_Result"">
        <part name=""parameters"" element=""tns:Update_Result""/>
    </message>
    <message name=""UpdateMultiple"">
        <part name=""parameters"" element=""tns:UpdateMultiple""/>
    </message>
    <message name=""UpdateMultiple_Result"">
        <part name=""parameters"" element=""tns:UpdateMultiple_Result""/>
    </message>
    <message name=""Delete"">
        <part name=""parameters"" element=""tns:Delete""/>
    </message>
    <message name=""Delete_Result"">
        <part name=""parameters"" element=""tns:Delete_Result""/>
    </message>
    <portType name=""SMLSync_Port"">
        <operation name=""ReadByRecId"">
            <input name=""ReadByRecId"" message=""tns:ReadByRecId""/>
            <output name=""ReadByRecId_Result"" message=""tns:ReadByRecId_Result""/>
        </operation>
        <operation name=""ReadMultiple"">
            <input name=""ReadMultiple"" message=""tns:ReadMultiple""/>
            <output name=""ReadMultiple_Result"" message=""tns:ReadMultiple_Result""/>
        </operation>
        <operation name=""IsUpdated"">
            <input name=""IsUpdated"" message=""tns:IsUpdated""/>
            <output name=""IsUpdated_Result"" message=""tns:IsUpdated_Result""/>
        </operation>
        <operation name=""GetRecIdFromKey"">
            <input name=""GetRecIdFromKey"" message=""tns:GetRecIdFromKey""/>
            <output name=""GetRecIdFromKey_Result"" message=""tns:GetRecIdFromKey_Result""/>
        </operation>
        <operation name=""Create"">
            <input name=""Create"" message=""tns:Create""/>
            <output name=""Create_Result"" message=""tns:Create_Result""/>
        </operation>
        <operation name=""CreateMultiple"">
            <input name=""CreateMultiple"" message=""tns:CreateMultiple""/>
            <output name=""CreateMultiple_Result"" message=""tns:CreateMultiple_Result""/>
        </operation>
        <operation name=""Update"">
            <input name=""Update"" message=""tns:Update""/>
            <output name=""Update_Result"" message=""tns:Update_Result""/>
        </operation>
        <operation name=""UpdateMultiple"">
            <input name=""UpdateMultiple"" message=""tns:UpdateMultiple""/>
            <output name=""UpdateMultiple_Result"" message=""tns:UpdateMultiple_Result""/>
        </operation>
        <operation name=""Delete"">
            <input name=""Delete"" message=""tns:Delete""/>
            <output name=""Delete_Result"" message=""tns:Delete_Result""/>
        </operation>
    </portType>
    <binding name=""SMLSync_Binding"" type=""tns:SMLSync_Port"">
        <binding xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" transport=""http://schemas.xmlsoap.org/soap/http""/>
        <operation name=""ReadByRecId"">
            <operation xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" soapAction=""urn:microsoft-dynamics-schemas/page/smlsync:ReadByRecId"" style=""document""/>
            <input name=""ReadByRecId"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </input>
            <output name=""ReadByRecId_Result"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </output>
        </operation>
        <operation name=""ReadMultiple"">
            <operation xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" soapAction=""urn:microsoft-dynamics-schemas/page/smlsync:ReadMultiple"" style=""document""/>
            <input name=""ReadMultiple"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </input>
            <output name=""ReadMultiple_Result"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </output>
        </operation>
        <operation name=""IsUpdated"">
            <operation xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" soapAction=""urn:microsoft-dynamics-schemas/page/smlsync:IsUpdated"" style=""document""/>
            <input name=""IsUpdated"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </input>
            <output name=""IsUpdated_Result"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </output>
        </operation>
        <operation name=""GetRecIdFromKey"">
            <operation xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" soapAction=""urn:microsoft-dynamics-schemas/page/smlsync:GetRecIdFromKey"" style=""document""/>
            <input name=""GetRecIdFromKey"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </input>
            <output name=""GetRecIdFromKey_Result"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </output>
        </operation>
        <operation name=""Create"">
            <operation xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" soapAction=""urn:microsoft-dynamics-schemas/page/smlsync:Create"" style=""document""/>
            <input name=""Create"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </input>
            <output name=""Create_Result"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </output>
        </operation>
        <operation name=""CreateMultiple"">
            <operation xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" soapAction=""urn:microsoft-dynamics-schemas/page/smlsync:CreateMultiple"" style=""document""/>
            <input name=""CreateMultiple"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </input>
            <output name=""CreateMultiple_Result"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </output>
        </operation>
        <operation name=""Update"">
            <operation xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" soapAction=""urn:microsoft-dynamics-schemas/page/smlsync:Update"" style=""document""/>
            <input name=""Update"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </input>
            <output name=""Update_Result"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </output>
        </operation>
        <operation name=""UpdateMultiple"">
            <operation xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" soapAction=""urn:microsoft-dynamics-schemas/page/smlsync:UpdateMultiple"" style=""document""/>
            <input name=""UpdateMultiple"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </input>
            <output name=""UpdateMultiple_Result"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </output>
        </operation>
        <operation name=""Delete"">
            <operation xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" soapAction=""urn:microsoft-dynamics-schemas/page/smlsync:Delete"" style=""document""/>
            <input name=""Delete"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </input>
            <output name=""Delete_Result"">
                <body xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" use=""literal""/>
            </output>
        </operation>
    </binding>
    <service name=""SMLSync_Service"">
        <port name=""SMLSync_Port"" binding=""tns:SMLSync_Binding"">
            <address xmlns=""http://schemas.xmlsoap.org/wsdl/soap/"" location=""http://navtest.sakuko.vn:7047/NAV_TEST/WS/FC_VPH_01/Page/SMLSync""/>
        </port>
    </service>
</definitions>
            ";

        try
        {
            // Create the HttpClient instance with authentication
            HttpClientHandler handler = new HttpClientHandler
            {
                Credentials = new NetworkCredential(username, password)
            };
            using (HttpClient httpClient = new HttpClient(handler))
            {
                // Create the HttpRequestMessage and set the headers
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, soapEndpoint);
                request.Content = new StringContent(soapRequestBody, Encoding.UTF8, "text/xml");

                // Send the request and get the response
                HttpResponseMessage response = httpClient.SendAsync(request).GetAwaiter().GetResult();
                Console.WriteLine("----------response HTTP---------");
                Console.WriteLine(response);
                Console.WriteLine("-------------------");
                // Check if the request was successful (HTTP status code 200)
                if (response.IsSuccessStatusCode)
                {
                    // Read the SOAP response
                    string soapResponse = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    //Console.WriteLine(soapResponse);
                    Console.WriteLine("----------SOAP Response---------");
                    Console.WriteLine(soapResponse);
                    Console.WriteLine("-------------------");

                    // Process the SOAP response (parsing, etc.)
                    // For example, you can use XmlDocument or XDocument to parse the XML response
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(soapResponse);

                    // Find the PostPO_Result node and extract its value
                    XmlNode postPOResultNode = xmlDoc.SelectSingleNode("/PostPO_Result");

                    Console.WriteLine("----------postPOResultNode---------");
                    Console.WriteLine(postPOResultNode);
                    Console.WriteLine("-------------------");

                    if (postPOResultNode != null)
                    {
                        string postPOResultValue = postPOResultNode.InnerText;
                        Console.WriteLine("----------SUCCESS---------");
                        Console.WriteLine("PostPO_Result: " + postPOResultValue);
                        Console.WriteLine("-------------------");
                    }
                    else
                    {
                        Console.WriteLine("PostPO_Result not found in SOAP response.");
                    }
                }
                else
                {
                    // Handle the case when the request was not successful
                    //Console.WriteLine("SOAP Request Failed. Status Code: " + response.StatusCode);
                    Console.WriteLine("----------FAILED---------");
                    Console.WriteLine("SOAP Request Failed. Status Code: " + response.StatusCode);
                    Console.WriteLine("-------------------");
                }
            }
        }
        catch (Exception ex)
        {
            // Handle exceptions appropriately
            //Console.WriteLine("Error: " + ex.Message);
            Console.WriteLine("----------FAILED---------");
            Console.WriteLine("Error: " + ex.Message);
            Console.WriteLine("-------------------");
        }

        return Ok("ok");
    }

    [HttpGet]
    [Route("test")]
    public IActionResult Test()
    {
        // string name = "hehe";
        // string result = _sakukoPartnerService.GetUsername(name);

        // return Ok(result);
        string cc = UtilityService.Text;

        return Ok(cc);
    }

    [HttpPost]
    [Route("SendGoodReceipt")]
    public async Task<PostPO_ResultResponse> sendGoodReceipt()
    {
        string soapReqBody;
        using (var content = new StreamContent(Request.Body))
        {
            soapReqBody = await content.ReadAsStringAsync();
        }

        return await _sakukoPartnerService.SendGoodReceipt(soapReqBody);
    }

    [HttpPost]
    [Route("SendGoodIssue")]
    public async Task<PostPO_ResultResponse> sendGoodIssue()
    {
        string soapReqBody;
        using (var content = new StreamContent(Request.Body))
        {
            soapReqBody = await content.ReadAsStringAsync();
        }

        return await _sakukoPartnerService.SendGoodReceipt(soapReqBody);
    }
}
