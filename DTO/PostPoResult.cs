using System.Net;
using System.Xml.Serialization;

namespace sakuko_api.DTO
{
    [XmlRoot(ElementName = "PostPO_Result", Namespace = "http://schemas.xmlsoap.org/wsdl/soap/")]
    public class PostPO_ResultResponse
    
    {
        [XmlElement(ElementName = "Code", Namespace = "http://schemas.xmlsoap.org/wsdl/soap/")]
        public int Code { get; set; }
        [XmlElement(ElementName = "Message", Namespace = "http://schemas.xmlsoap.org/wsdl/soap/")]
        public string Message { get; set; }

        public HttpStatusCode StatusCode { get; set; }
    }
}